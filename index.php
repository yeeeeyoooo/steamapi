<!DOCTYPE html>
<html lang="en" dir="ltr">
  <head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <meta charset="utf-8">
    <title>Test api steam</title>
  </head>
  <body>
    <h1>Videogames with API request</h1>
    <button type="button" id="btnGames">Display list</button>
    <div id="divGames">
    <table id="tabGames">
      <thead>
        <tr>
          <th colspan="2">List of steam games</th>
        </tr>
        <tbody id="bodyGames">
        <tbody>
      <script>
      $(document).ready(function(){
        $('#btnGames').click(function(e) {
          e.preventDefault();
          $.ajax({
            url: './view/viewApi.php',
            type: 'GET',
            datatype: 'json',
            success: function(data) {
                var res = jQuery.parseJSON(data);
                for(var i=0; i<res.length; i++) {
                  $('#bodyGames').append('<tr><td>' + res[i].appid + '</td>' + '<td>' + res[i].name) + '</td>';
                }
            }
          });
        });
      });
      </script>
    </div>
  </body>
</html>
