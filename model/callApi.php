<?php

require(__DIR__ . "/../public/const.php");

function callApi($url) 
{
    $key = API_KEY;
    $steamid = STEAMID;

    $url = $url . "?key=$key&include_appinfo=1&steamid=$steamid&format=json";

    $ch = curl_init();

    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);

    $result = curl_exec($ch);

    if(!$result) 
    {
        die("Connection Failure");
    }

    curl_close($ch);

    //tranforming results in array
    $result_array = json_decode($result, true);

    //indexes to filter
    $keys = array('appid', 'name');

    //Render array
    $final_array = array();

    //for each sub array, filtering with what we want
    foreach($result_array['response']['games'] as $res)
    {
        $filtered_array = array_filter(
            $res,
            function($key) use ($keys) {
                return in_array($key, $keys);
            },
            ARRAY_FILTER_USE_KEY
        );

        array_push($final_array, $filtered_array);
    }

    echo json_encode($final_array);

}

?>
